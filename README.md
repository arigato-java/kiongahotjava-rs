# 今日も気温がHotJava

気温が28℃以上でマストドンに投稿します。

## しくみ

1. 気象庁のJSONからよむ
1. 28度以上かか判定する（ジャバ・ノットジャバ判定）
1. マストドンに投稿する

## しくみのしくみ

* 今回はGoogle Cloud Runでうごきます
* 実装言語も流行りに乗ってRustにしました

## 設定のひみつ

### 環境変数

| なまえ | やくわり | 設定例 |
| ------ | ------ | ----- |
| JMA_URL | 気象庁のJSONのURL | https://www.jma.go.jp/bosai/forecast/data/forecast/260000.json |
| JMA_LOCATION | 気温をJSON抽出するための地域ID | 61286 |
| MASTODON_URL | Mastodonの投稿用URL | https://mstdn.jp/api/v1/statuses |
| MASTODON_TOKEN | Mastodonのトークン(ひみつ情報) | abc_deFgHijk |
