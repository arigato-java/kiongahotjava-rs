const HOTJAVA_STANDARD_MSG: &str = "今日も気温がHotJava";
const HOTJAVA_HIGH_MSG: &str = "今日の気温がモットジャバ";

pub fn hotjava(temperature: Option<f64>) -> Option<&'static str> {
    match temperature {
        None => None,
        Some(t) if t < 28.0 => None,
        Some(t) if t < 37.0 => Some(HOTJAVA_STANDARD_MSG),
        _ => Some(HOTJAVA_HIGH_MSG),
    }
}

#[test]
fn test_hotjava() {
    assert!(hotjava(Some(16.0)) == None);
    assert!(hotjava(Some(29.0)) == Some(HOTJAVA_STANDARD_MSG));
    assert!(hotjava(Some(128.0)) == Some(HOTJAVA_HIGH_MSG));
}
