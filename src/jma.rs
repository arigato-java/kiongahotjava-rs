// 気象庁のJSONから気温を持ってくる
use chrono::offset::Utc;
use chrono::{DateTime, FixedOffset};
use reqwest::IntoUrl;
use reqwest_middleware::ClientWithMiddleware;
use serde::Deserialize;
use serde_json::from_str;

#[derive(Deserialize, Debug)]
#[serde(transparent)]
pub struct JmaForecasts {
    forecasts: Vec<JmaForecast>,
}
#[derive(Deserialize, Debug)]
#[allow(non_snake_case)]
struct JmaForecast {
    timeSeries: Vec<JmaTimeserie>,
}
#[derive(Deserialize, Debug)]
#[allow(non_snake_case)]
struct JmaTimeserie {
    timeDefines: Vec<String>,
    areas: Vec<JmaArea>,
}
#[derive(Deserialize, Debug)]
#[allow(non_snake_case)]
struct JmaArea {
    area: JmaAreaDef,
    temps: Option<Vec<String>>,
}
#[derive(Deserialize, Debug)]
struct JmaAreaDef {
    code: String,
}

pub async fn jma_fetch<U: IntoUrl>(
    client: &ClientWithMiddleware,
    url: U,
) -> Result<JmaForecasts, reqwest_middleware::Error> {
    let response = client.get(url).send().await?.json::<JmaForecasts>().await?;
    Ok(response)
}

pub fn get_temperature(forecasts: &JmaForecasts, location: &str) -> Option<f64> {
    for f in forecasts.forecasts.iter() {
        for t in f.timeSeries.iter() {
            for a in t.areas.iter() {
                if a.area.code == location && a.temps.is_some() {
                    let temps = a.temps.as_ref().unwrap();
                    let today_indices = get_today_indices(&t.timeDefines);
                    let mut res = None;
                    for i in today_indices.iter() {
                        if i >= &temps.len() {
                            continue;
                        }
                        let tempi_res = from_str(&temps[*i]);
                        if let Ok(tempi) = tempi_res {
                            res = match res {
                                Some(temp) => {
                                    if temp < tempi {
                                        Some(tempi)
                                    } else {
                                        res
                                    }
                                }
                                None => Some(tempi),
                            }
                        }
                    }
                    if res.is_some() {
                        return res;
                    }
                }
            }
        }
    }
    None
}

fn get_today_indices(time_defines: &[String]) -> Vec<usize> {
    let hour = 3600;
    let jst_offset = FixedOffset::east_opt(9 * hour).unwrap();
    let todays_date = Utc::now().with_timezone(&jst_offset).date_naive();

    let mut res = Vec::with_capacity(time_defines.len());
    for (i, t) in time_defines.iter().enumerate() {
        let dt = DateTime::parse_from_rfc3339(t);
        if let Ok(dn) = dt {
            if dn.with_timezone(&jst_offset).date_naive() == todays_date {
                res.push(i)
            }
        }
    }
    res
}
