use reqwest_middleware::ClientBuilder;
use reqwest_retry::{policies::ExponentialBackoff, RetryTransientMiddleware};

mod jma;
use jma::*;
mod mastodon;
use mastodon::*;
mod hotjava;
use hotjava::*;

const USER_AGENT_STR: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let retry_policy = ExponentialBackoff::builder().build_with_max_retries(2);
    let req_client = ClientBuilder::new(
        reqwest::Client::builder()
            .user_agent(USER_AGENT_STR)
            .build()
            .unwrap(),
    )
    .with(RetryTransientMiddleware::new_with_policy(retry_policy))
    .build();

    let jma_url = std::env::var("JMA_URL");
    if jma_url.is_err() || jma_url.as_ref().unwrap().is_empty() {
        println!("JMA_URL not set");
        return;
    }
    let forecasts = jma_fetch(&req_client, &jma_url.unwrap()).await.unwrap();

    let jma_location = std::env::var("JMA_LOCATION");
    if jma_location.is_err() || jma_location.as_ref().unwrap().is_empty() {
        println!("JMA_LOCATION not set.\nforecasts: {:?}", forecasts);
        return;
    }
    let temperature = get_temperature(&forecasts, &jma_location.unwrap());
    if temperature.is_none() {
        println!("Temprature forecast unavailable. Try running this program in the morning.");
        return;
    }

    let message = hotjava(temperature);
    if message.is_none() {
        println!("Today is Not Java. Temperature: {}", temperature.unwrap());
        return;
    }

    let mastodon_url = std::env::var("MASTODON_URL");
    if mastodon_url.is_err() || mastodon_url.as_ref().unwrap().is_empty() {
        println!(
            "MASTODON_URL not set. Temperature: {}",
            temperature.unwrap()
        );
        return;
    }
    mastodon_post(&req_client, &mastodon_url.unwrap(), message.unwrap())
        .await
        .unwrap();
}
