use rand::{rng, Rng};
use reqwest::{header::AUTHORIZATION, IntoUrl};
use reqwest_middleware::ClientWithMiddleware;
use serde::Serialize;
use thiserror::Error;

#[derive(Serialize)]
struct MastodonMessage {
    status: String,
}

#[derive(Error, Debug)]
pub enum MastodonPostError {
    #[error("Middleware error: {0}")]
    Middleware(#[from] reqwest_middleware::Error),
    #[error("Serde json error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("Reqwest error: {0}")]
    Reqwest(#[from] reqwest::Error),
}

pub async fn mastodon_post<U: IntoUrl>(
    client: &ClientWithMiddleware,
    url: U,
    message: &str,
) -> Result<(), MastodonPostError> {
    let request_obj = MastodonMessage {
        status: message.to_string(),
    };
    let mastodon_token = std::env::var("MASTODON_TOKEN");
    if mastodon_token.is_err() || mastodon_token.as_ref().unwrap().is_empty() {
        println!("MASTODON_TOKEN not set.\nBody: {}", request_obj.status);
        return Ok(());
    }

    let mut rng = rng();
    let idempotency_key: u64 = rng.random();
    let response = client
        .post(url)
        .header(
            AUTHORIZATION.as_str(),
            format!("Bearer {}", mastodon_token.unwrap()),
        )
        .header("Idempotency-Key", format!("{}", idempotency_key))
        .json(&request_obj)
        .send()
        .await?
        .text()
        .await?;
    println!("Mastodon response: {}", response);
    Ok(())
}
